library(stackr) # devtools::install_github("dgrtwo/stackr")
library(gh) # devtools::install_github("r-lib/gh")
library(ggalt)
library(hrbrthemes)
library(tidyverse)

# StackOverflow! ----------------------------------------------------------

stack_user <- 1457051

my_answers <- stack_users(
  stack_user, "answers",
  fromdate=as.integer(as.POSIXct(as.Date("2016-01-01"))),
  todate=as.integer(as.POSIXct(as.Date("2017-12-31"))),
  pagesize=100,
  num_pages=50
)

my_comments <- stack_users(
  stack_user, "comments",
  fromdate=as.integer(as.POSIXct(as.Date("2016-01-01"))),
  todate=as.integer(as.POSIXct(as.Date("2017-12-31"))),
  pagesize=100,
  num_pages=50
)

my_badges <- stack_users(
  stack_user, "badges",
  fromdate=as.integer(as.POSIXct(as.Date("2016-01-01"))),
  todate=as.integer(as.POSIXct(as.Date("2017-12-31"))),
  pagesize=100,
  num_pages=30
)

my_rep <- stack_users(
  stack_user, "reputation-history",
  pagesize=100,
  num_pages=100
)

list(
  my_answers = my_answers,
  my_comments = my_comments,
  my_badges = my_badges,
  my_rep = my_rep
) -> my_so

#write_rds(my_so, "my_so.rds")

# GitHub! -----------------------------------------------------------------

s_ghn <- safely(gh_next)

curr_repo <- gh("/user/repos", username = "public")
my_repos <- list()
i <- 1
my_repos[i] <- list(curr_repo)
spin <-  TRUE
while(spin) {
  curr_repo <- s_ghn(curr_repo)
  if (is.null(curr_repo$result)) break
  i <- i + 1
  curr_repo <- curr_repo$result
  my_repos[i] <- list(curr_repo)
}

my_repos <- unlist(my_repos, recursive=FALSE)

#write_rds(my_repos, "my_repos.rds")
my_repos <- read_rds("my_repos.rds")

public_repos <- keep(my_repos, ~.x$owner$login == "hrbrmstr" & !.x$private)
public_repo_count <- length(public_repos)

map_df(public_repos, ~{
  data_frame(
    name = .x$name,
    created = anytime::anytime(.x$created_at),
    updated = anytime::anytime(.x$updated_at),
    stars = .x$stargazers_count,
    watchers = .x$watchers_count,
    lang = .x$language %||% NA_character_
  )
}) -> repo_meta

mutate(repo_meta) %>%
  mutate(days_alive = ceiling(as.numeric(updated - created, "days"))) %>%
  top_n(20, wt=stars) %>%
  arrange(desc(stars)) -> top_20

# Punch cards (top 20) ----------------------------------------------------

pull(top_20, name) %>%
  map(~{
    gh("/repos/:owner/:repo/stats/punch_card", owner="hrbrmstr", repo=.x)
  }) -> punch_cards

#write_rds(punch_cards, "punch_cards.rds")
punch_cards <- read_rds("punch_cards.rds")

n <- if (length(punch_cards) > 20) 20 else length(punch_cards)

map_df(1:n, ~{
  map_df(punch_cards[[.x]], ~set_names(.x, c("day", "hour", "Commits"))) %>%
    mutate(repo = top_20$name[.x])
}) %>%
  mutate(repo = factor(repo, levels=unique(repo))) -> punch_cards_df

ggplot(punch_cards_df, aes(hour, day, fill=Commits)) +
  geom_tile(color="#b2b2b2", size=0.125) +
  scale_x_continuous(expand=c(0,0), breaks=c(0, 12, 23), labels=sprintf("%02d:00", c(0, 12, 23))) +
  scale_y_continuous(expand=c(0,0), breaks=0:6, labels=c("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")) +
  viridis::scale_fill_viridis() +
  facet_wrap(~repo, ncol=4) +
  coord_fixed() +
  labs(x=NULL, y=NULL, title="Punch cards (2017 commits)") +
  theme_ipsum_rc(grid="", axis_text_size=7, strip_text_face="bold") +
  theme(axis.text.x=element_text(hjust=c(0, 0.5, 1)))

count(punch_cards_df, day, hour, wt=Commits) %>%
  ggplot(aes(hour, day, fill=n)) +
  geom_tile(color="#b2b2b2", size=0.125) +
  scale_x_continuous(expand=c(0,0), breaks=c(0, 6, 12, 18, 23), labels=sprintf("%02d:00", c(0, 6, 12, 18, 23))) +
  scale_y_continuous(expand=c(0,0), breaks=0:6, labels=c("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")) +
  viridis::scale_fill_viridis(name="Commits") +
  coord_fixed() +
  labs(x=NULL, y=NULL, title="Top 20 Combined Punch Cards (2017 Commits)") +
  theme_ipsum_rc(grid="", strip_text_face="bold")

# Weekly commit pulse (top 20) --------------------------------------------

pull(top_20, name) %>%
  map(~{
    gh("/repos/:owner/:repo/stats/commit_activity", owner="hrbrmstr", repo=.x)
  }) -> repo_activity

#write_rds(repo_activity, "repo_activity.rds")
repo_activity <- read_rds("repo_activity.rds")

n <- if (length(repo_activity) > 20) 20 else length(repo_activity)

map_df(1:n, ~{
  map_df(repo_activity[[.x]], ~{
    .x$days <- set_names(.x$days, c("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"))
    flatten(.x)
  }) %>%
    mutate(repo = top_20$name[.x])
}) %>%
  mutate(week = anytime::anydate(week)) %>%
  mutate(repo = factor(repo, levels=unique(repo))) %>%
  select(-total) %>%
  gather(day, commits, -week, -repo) %>%
  mutate(day = factor(day, levels=c("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"))) -> repo_activity_df

wk_cnt_df <- count(repo_activity_df, repo, week, wt=commits)

ggplot(wk_cnt_df, aes(week, n, group=repo, fill=repo)) +
  stat_xspline(aes(color=repo, fill=repo), size=1, geom="area", alpha=2/3) +
  scale_x_date(expand=c(0,0),
               breaks=c(seq(as.Date("2017-01-01"), as.Date("2017-12-31"), "3 months"), as.Date("2017-12-31")),
               limits=as.Date(c("2017-01-01", "2018-01-01")),
               date_labels="%b\n%Y") +
  scale_y_continuous(expand=c(0,0)) +
  ggthemes::scale_color_tableau(palette="tableau20") +
  ggthemes::scale_fill_tableau(palette="tableau20") +
  facet_wrap(~repo, ncol=1, strip.position="left") +
  labs(x=NULL, y=NULL, title="Top 20 Weekly Commit Pulse (owner, 2017)",
       caption=sprintf("Y-axis scale max is %s commits", scales::comma(max(wk_cnt_df$n)))) +
  theme_ipsum_rc(grid="X", axis_text_size=11, strip_text_size=11) +
  theme(panel.background=element_rect(color="#00000000", fill="#00000000")) +
  theme(panel.spacing.y=unit(-10, "pt")) +
  theme(axis.text.y=element_blank()) +
  theme(axis.ticks.y=element_blank()) +
  theme(strip.text.y=element_text(hjust=1, vjust = -0.25, face="bold", angle=360)) +
  theme(legend.position="none") -> gg

gb <- ggplot_build(gg)
gt <- ggplot_gtable(gb)
tcols <- ggthemes::tableau_color_pal("tableau20")(20)
strip_grobs <- which(grepl("strip", gt$layout$name))

for (i in 1:20) {
  gt$grobs[strip_grobs[i]][[1]]$grobs[[1]]$children[[2]]$children[[1]]$gp$col <- tcols[i]
}

grid::grid.newpage()
grid::grid.draw(gt)


# New repos per week ------------------------------------------------------

just_2017 <- filter(repo_meta, lubridate::year(created) == 2017)

mutate(just_2017, week=lubridate::week(created)) %>%
  count(week) -> new_repos_per_week

ggplot(new_repos_per_week, aes(week, n)) +
  geom_ubar(size=1) +
  scale_y_comma(limits=range(0, max(new_repos_per_week$n))) +
  scale_x_comma(limits=c(1,52), breaks=c(1, seq(0, 52, 4))) +
  labs(x="Week (2017)", y="# New Repos", title="New Repos per Week (2017)") +
  theme_ipsum_rc(grid="Y")




